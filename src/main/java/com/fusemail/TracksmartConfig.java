package main.java.com.fusemail;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TracksmartConfig {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TracksmartConfig.class);
	
	private static final TracksmartConfig instance = new TracksmartConfig();
	private static String PROPERTIES_FILE = "../config/config.properties";
	
	private static Properties config = new Properties();
	
	private TracksmartConfig() {
		try (InputStream is = new FileInputStream(PROPERTIES_FILE)) {
            Properties config = new Properties();
            config.load(is);
		}
		catch (FileNotFoundException e ) {
            LOGGER.error("Failed loading properties from file {}; file not found", PROPERTIES_FILE);
            System.exit(1);
        }
		catch (IOException e) {
            LOGGER.error("Failed loading properties from the file {}; error: {}", PROPERTIES_FILE, e.getMessage());
            System.exit(1);
        }
	}
	
	public static TracksmartConfig getInstance() {
        return instance;
    }
	
	public String getProperty(String key) {
		return config.containsKey(key) ? config.getProperty(key) : "";
    }
	
	public boolean isPoolEnabled() {
		return config.containsKey("CONN_POOL_LIB");
	}
	
	
	

}
