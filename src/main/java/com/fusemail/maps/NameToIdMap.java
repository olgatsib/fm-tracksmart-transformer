package main.java.com.fusemail.maps;

import java.sql.SQLException;
import java.util.Map;

import main.java.com.fusemail.exceptions.DatabaseException;
import main.java.com.fusemail.maps.query.DbFetcher;

public abstract class NameToIdMap {
	
	private static final String READ_DB_HOST_KEY = "SHARD_READ_DB_HOST";
	private static final String READ_DB_NAME_KEY = "SHARD_READ_DB_NAME";
	private static final String WRITE_DB_HOST_KEY = "SHARD_WRITE_DB_HOST";
	private static final String WRITE_DB_NAME_KEY = "SHARD_WRITE_DB_NAME";
	
	public int getId(String name, Map<String, Integer> cache) {
		try {
			if (cache.isEmpty()) {
				loadCache();
			}
			if (cache.containsKey(name)) {
				return cache.get(name);
			}
			else {
				int id = getIdFromDb(name);
				if (id == -1) {
					id = createNewId(name);
					cache.put(name, id);
				}
			}
			return cache.get(name);
		}
		catch (SQLException e) {
			throw new DatabaseException(e.getMessage(), e);
		}
	}
	
	protected void loadCache() throws SQLException {
		DbFetcher dbFetcher = new DbFetcher(READ_DB_HOST_KEY, READ_DB_NAME_KEY);
		Map<String, Integer> map = dbFetcher.fetchTableFromDb(this);
		setCache(map);
	}
	
	protected int getIdFromDb(String name) throws SQLException {
		DbFetcher dbFetcher = new DbFetcher(READ_DB_HOST_KEY, READ_DB_NAME_KEY);
		return dbFetcher.fetchIdFromDb(this, name);
	}
	
	protected void saveIdToDb(String name, int id) {
		
	}
	
	protected abstract int createNewId(String name);
	protected abstract void setCache(Map<String, Integer> map);
	public abstract String getTableName();
	public abstract boolean isNameCaseSensitive();
}
