package main.java.com.fusemail.maps.query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import main.java.com.fusemail.database.Database;
import main.java.com.fusemail.maps.NameToIdMap;

public class DbFetcher extends DbAbstract {
	
	private static final String SQL_FETCH_ID_QUERY = "SELECT id FROM %s WHERE %s name = ?";
	private static final String SQL_FETCH_TABLE_QUERY = "SELECT name, id FROM %s";
	
	private final String dbHostKey;
	private final String dbNameKey;
	
	public DbFetcher(String dbHostKey, String dbNameKey) {
		this.dbHostKey = dbHostKey;
		this.dbNameKey = dbNameKey;
	}
	
	public int fetchIdFromDb(NameToIdMap item, String name) throws SQLException {
		String sql = createQuery(item, name);
		try (Connection conn = Database.getInstance().getConnection(dbHostKey, dbNameKey);
				PreparedStatement ps = createPreparedStatement(conn, sql, name);
				ResultSet rs = ps.executeQuery()) {
			
			if (rs.next()) {
	            return rs.getInt(1);
			}
		}
		return -1;
	}
	
	public Map<String, Integer> fetchTableFromDb(NameToIdMap item) throws SQLException {
		Map<String, Integer> map = new HashMap<>();
		String sql = createQuery(item);
		try (Connection conn = Database.getInstance().getConnection(dbHostKey, dbNameKey);
				PreparedStatement ps = conn.prepareStatement(sql);
				ResultSet rs = ps.executeQuery()) {
			
			while (rs.next()) {
	            map.put(rs.getString("name"), rs.getInt("id"));
			}
		}
		return map;
	}
	
	private PreparedStatement createPreparedStatement(Connection conn, String sqlStatement, String name)
            throws SQLException {
        return createPreparedStatement(conn, sqlStatement, name, 1);
    }
	
	private String createQuery(NameToIdMap item, String name) {
		return String.format(SQL_FETCH_ID_QUERY, item.getTableName(), item.isNameCaseSensitive());
    }
	
	private String createQuery(NameToIdMap item) {
		return String.format(SQL_FETCH_TABLE_QUERY, item.getTableName());
    }

}
