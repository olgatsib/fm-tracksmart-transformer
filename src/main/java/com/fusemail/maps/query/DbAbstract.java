package main.java.com.fusemail.maps.query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DbAbstract {
	
	/**
	 * @param conn
	 * @param sqlStatement
	 * @param name
	 * @param numRepeat		indicates how many times to insert the value. 
	 * 						It is usually 1, if using Reverse for insertion, 
	 * 						it will be 2 (the value and its reverse)
	 * @return
	 * @throws SQLException
	 */
	protected PreparedStatement createPreparedStatement(Connection conn, String sqlStatement, 
			String name, int numRepeat) throws SQLException {
        PreparedStatement ps = conn.prepareStatement(sqlStatement);
        int count = 0;
        while (count < numRepeat) {
            count++;
            ps.setString(count, name);
        }
        return ps;
    }
}
