package main.java.com.fusemail.maps;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Tls extends NameToIdMap {
	
	private static final String TLS_TABLE = "tls_id";
	
	private final Map<String, Integer> tlsCache = new ConcurrentHashMap<>();
			
	public int getId(String name) {
		return getId(name, tlsCache);
	}
	
	@Override
	protected int createNewId(String name) {
		return 1;
	}
	
	@Override
	public String getTableName() {
		return TLS_TABLE;
	}
	
	@Override
	public boolean isNameCaseSensitive() {
		return false;
	}
	
	
	protected void setCache(Map<String, Integer> map) {
		tlsCache.putAll(map);
	}
}
