package main.java.com.fusemail.maps;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;

import com.fusemail.commons.utils.TextUtils;

public class Domain extends NameToIdMap {
	
	private final Map<String, Integer> domainCache = new ConcurrentHashMap<>();
	
	private static final String DOMAIN_TABLE = "domain_id";
	
	public int getId(Integer type, String sender, String recipient) {
		validateType(type);
		String domain = type == 0 ? getDomain(sender) : getDomain(recipient);
		return getId(domain, domainCache);
	}
	
	private void validateType(Integer type) {
		if (! (type == 0 && type == 1)) {
			throw new NoSuchElementException(String.format("Type %d doesn't exist", type));
		}
	}
	
	private String getDomain(String address) {
		int indexAt = address.indexOf("@");
		if (indexAt < 1) {
			throw new IllegalArgumentException("Wrong address format");
		}
		String domain = address.substring(indexAt + 1);
		if (TextUtils.isEmpty(domain)) {
			throw new IllegalArgumentException("Wrong address format");
		}
		return domain;
	}
	
	@Override
	protected int createNewId(String name) {
		return 1;
	}
	
	@Override
	public String getTableName() {
		return DOMAIN_TABLE;
	}
	
	@Override
	public boolean isNameCaseSensitive() {
		return false;
	}

	@Override
	protected void setCache(Map<String, Integer> map) {
		// TODO Auto-generated method stub
		
	}
}
