package main.java.com.fusemail;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fusemail.Transformer;

public class Tracksmart {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Tracksmart.class);
	
	public static void main(String[] args) {
		if (args.length != 1) {
            usage();
            System.exit(1);
        }

        String inputFile = args[0];
        LOGGER.info("input file: {}", inputFile);
        
        ExecutorService executor = Executors.newFixedThreadPool(1);
        Future<String> result = executor.submit(new Transformer(inputFile));
        try {
        	System.out.println(result.get());
        }
        catch(ExecutionException | InterruptedException e) {
        	LOGGER.info("Exception while processing input: {}", e.getMessage());
        	e.printStackTrace();
        }
	}
	
	private static void usage() {
        System.out.println("usage: Tracksmart input");
        System.out.println("\tinput: location of the input JSON file");
    }

}
