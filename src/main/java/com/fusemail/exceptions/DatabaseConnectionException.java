package main.java.com.fusemail.exceptions;

public class DatabaseConnectionException extends DatabaseException {
	
	private static final long serialVersionUID = 1L;

	public DatabaseConnectionException() {
        super();
    }

    public DatabaseConnectionException(String message) {
        super(message);
    }

    public DatabaseConnectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public DatabaseConnectionException(Throwable cause) {
        super(cause);
    }
}
