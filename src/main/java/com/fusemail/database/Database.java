package main.java.com.fusemail.database;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import main.java.com.fusemail.TracksmartConfig;
import main.java.com.fusemail.exceptions.DatabaseConnectionException;

public class Database {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Database.class);
	private static final String DB_PORT_KEY = "SHARD_READ_DB_PORT";
	private static final String DB_USER_KEY = "SHARD_READ_DB_USER";
    private static final String DB_PASSWORD_KEY = "SHARD_READ_DB_PASSWORD";
	
    private static Database instance = null;
    private final String port;
	private final String user;
    private final String password;
    
    private static TracksmartConfig config = null;
	private final DatabaseConnection dbConnection;
	
	private Database() {
		config = TracksmartConfig.getInstance();
		port = config.getProperty(DB_PORT_KEY);
		user = config.getProperty(DB_USER_KEY);
	    password = config.getProperty(DB_PASSWORD_KEY);
	    
		if (config.isPoolEnabled()) {
			dbConnection = new HikariPoolConnection(config);
		}
		else {
			dbConnection = new JdbcConnection(config);
		}
	}
	
	public static Database getInstance() {
		if (instance == null) {
			instance = new Database();
		}
		return instance;
	}
			
	public Connection getConnection(String hostKey, String dbKey) {
		String host = config.getProperty(hostKey);
		String db = config.getProperty(dbKey);
		return getConnection(host, port, db, user, password);
	}
	
	public Connection getConnection(String host, String port, String db, String user, String password) {
		Connection conn = null;
        try {
        	conn = dbConnection.getConnection(host, port, db, user, password);
        	LOGGER.debug("DB connection to [{}.{} is established", host, db);
        } 
        catch (SQLException e) {
        	throw new DatabaseConnectionException(e.getMessage(), e);
        }
        return conn;
	}
}
