package main.java.com.fusemail.database;

import java.sql.Connection;
import java.sql.SQLException;

import main.java.com.fusemail.TracksmartConfig;

public abstract class DatabaseConnection {
	
	protected TracksmartConfig tracksmartConfig;
	
	public DatabaseConnection(TracksmartConfig tracksmartConfig) {
		this.tracksmartConfig = tracksmartConfig;
	}
	
	public abstract Connection getConnection(String host, String port, String db, String user, String password) 
			throws SQLException;
	
	protected String getConnectionKey(String host, String db) {
        return host + (db == null ? "null" : db);
    }
}
