package main.java.com.fusemail.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import main.java.com.fusemail.TracksmartConfig;
import main.java.com.fusemail.exceptions.DatabaseConnectionException;

public class JdbcConnection extends DatabaseConnection {
	
	private static final Map<String, Connection> ALL_JDBC_CONNECTIONS = new ConcurrentHashMap<>();
	
	public JdbcConnection(TracksmartConfig config) {
		super(config);
	}
	
	public Connection getConnection(String host, String port, String db, String user, String password) 
			throws SQLException {
		
		String connKey = getConnectionKey(host, db);
		Connection conn = null;
        if (ALL_JDBC_CONNECTIONS.containsKey(connKey)) {
            conn = ALL_JDBC_CONNECTIONS.get(connKey);
        }
        if (conn == null || conn.isClosed()) {
            ALL_JDBC_CONNECTIONS.remove(connKey);
        }

        conn = getConnectionByJdbcDriver(host, port, db, user, password);
        ALL_JDBC_CONNECTIONS.put(connKey, conn);
		return conn;
	}
	
	private static Connection getConnectionByJdbcDriver(String host, String port, String db, String user, String password) {
    	try {
	        Class.forName("org.mariadb.jdbc.Driver");
	        StringBuilder jdbcStr = new StringBuilder("jdbc:mysql://");
	        jdbcStr.append(host).append(":").append(port).append("/");
	        if (db != null) {
	            jdbcStr.append(db);
	        }
	        jdbcStr.append("?" + "user=").append(user).append("&password=").append(password).append("&useUnicode=yes&characterEncoding=UTF-8");
	        return DriverManager.getConnection(jdbcStr.toString());
    	}
    	catch (ClassNotFoundException | SQLException e) {
    		throw new DatabaseConnectionException(e.getMessage(), e);
    	}
	}
}
