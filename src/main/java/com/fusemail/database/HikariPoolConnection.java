package main.java.com.fusemail.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import main.java.com.fusemail.TracksmartConfig;

public class HikariPoolConnection extends DatabaseConnection {
	
	private static final Map<String, HikariDataSource> ALL_HIKARI_POOLS = new ConcurrentHashMap<>();
	
	public HikariPoolConnection(TracksmartConfig config) {
		super(config);
	}
	
	public Connection getConnection(String host, String port, String db, String user, String password) 
			throws SQLException {
		String connKey = host;
        HikariDataSource ds = ALL_HIKARI_POOLS.get(connKey);
        if (ds == null) {
            ds = getHikariDataSource(host, port, user, password, connKey);
        }
        Connection conn = ds.getConnection();
        if (db != null) {
        	conn.setCatalog(db);
        }
        return conn;
	}
	
	private synchronized HikariDataSource getHikariDataSource(String host, String port, String user, String password, String connKey) {
        HikariDataSource ds = getHikariConnectionPool(host, port, user, password);
        ALL_HIKARI_POOLS.put(connKey, ds);
        return ds;
    }
    
    private HikariDataSource getHikariConnectionPool(String host, String port, String user, String password) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://" + host + ":" + port);
        config.setUsername(user);
        config.setPassword(password);
        config.addDataSourceProperty("cachePrepStmts", tracksmartConfig.getProperty("POOL_CACHE_PREP_STMTS"));
        config.addDataSourceProperty("prepStmtCacheSize", tracksmartConfig.getProperty("POOL_PREP_STMT_CACHE_SIZE"));
        config.addDataSourceProperty("prepStmtCacheSqlLimit", tracksmartConfig.getProperty("POOL_PREP_STMT_CACHE_SQL_LIMIT"));
        config.addDataSourceProperty("useServerPrepStmts", tracksmartConfig.getProperty("POOL_USE_SERVER_PREP_STMTS"));
        config.addDataSourceProperty("maxLifetime", tracksmartConfig.getProperty("POOL_MAX_LIFETIME_MS"));
        config.addDataSourceProperty("idleTimeout", tracksmartConfig.getProperty("POOL_IDLE_TIMEOUT_MS"));
        config.setConnectionTimeout(Long.valueOf(tracksmartConfig.getProperty("POOL_CONNECTION_TIMEOUT_MS")));
        config.setLeakDetectionThreshold(Long.valueOf(tracksmartConfig.getProperty("POOL_LEAK_DETECTION_THRESHOLD")));
        config.setPoolName(tracksmartConfig.getProperty("POOL_NAME_PREFIX") + "." + host);
        config.setMaximumPoolSize(Integer.parseInt(tracksmartConfig.getProperty("POOL_MAX_SIZE")));
        return new HikariDataSource(config);
    }
}
